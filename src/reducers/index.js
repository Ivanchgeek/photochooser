import { combineReducers } from 'redux'

import views from "./views"
import files from "./files"

export default combineReducers({
    views,
    files
})
