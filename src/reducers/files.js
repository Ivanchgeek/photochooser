const initialState = {
    files: [],
    count: 0,
    limit: 1,
    active: 0
}

export default function fullscreen(state = initialState, action) {
    switch (action.type) {
        case "SetFiles":
            return (
                {
                    ...state,
                    files: action.value,
                    count: action.value.length
                }
            )
        case "SetLimit":
            return (
                {
                    ...state,
                    limit: action.value
                }
            )
        case "NextImage":
            return (
                {
                    ...state,
                    active: state.active === state.limit - 1 ? 0 : state.active + 1
                }
            )
        case "PrevImage":
            return (
                {
                    ...state,
                    active: state.active === 0 ? state.limit - 1 : state.active - 1
                }
            )
        case "RemoveActive":
            return (
                {
                    ...state,
                    count: state.count - 1,
                    active: state.active === state.count - 1 ? 0 : state.active,
                    files: [...state.files.slice(0, state.active), ...state.files.slice(state.active+1)]
                }
            )
        default:
            return state
    }
}
