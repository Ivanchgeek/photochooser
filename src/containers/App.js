import "../styles/App.sass"

import { useSelector } from 'react-redux';

import Upload from "./views/Upload";
import Settings from "./views/Settings";
import Teaching from "./views/Teaching";
import Proccess from "./views/Proccess";
import Result from "./views/Result"

const App = (props) => {

  const viewCode = useSelector(state => state.views.view)

  function view() {
    switch (viewCode) {
      case "upload": return (
        <Upload></Upload>
      )
      case "settings": return (
        <Settings></Settings>
      )
      case "teaching": return (
        <Teaching></Teaching>
      )
      case "proccess": return (
        <Proccess></Proccess>
      )
      case "result": return (
        <Result></Result>
      )
      default: return (
        <h1>no such view</h1>
      )
    }
  }

  return (
    <>
      {view()}
    </>
  );
}

export default App