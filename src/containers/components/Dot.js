import React from 'react';
import {useSelector} from 'react-redux';

const Dot = (props) => {

    const active = useSelector(state => state.files.active)

    return (
        <div className={`dot ${props.index === active ? "active" : ""}`} style={{transform: `rotate(${(props.index + props.max - active) / props.max * 360}deg)`}}></div>
    )
}

export default Dot