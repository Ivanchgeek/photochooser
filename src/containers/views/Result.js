import React, { useEffect, useRef, useState } from 'react';
import {useSelector} from 'react-redux';

const Result = () => {

    const self = useRef(null)

    const [loaded, setLoaded] = useState(false)

    const files = useSelector(state => state.files.files)

    useEffect(() => {
        setLoaded(true)
        setTimeout(() => {
            self.current.style.opacity = "1"
        }, 10)
    }, [loaded])

    function copyTextToClipboard() {
        var text = ""
        files.forEach(f => {
            text += f.name + "\n"
        })
        console.log(text)
        if (!navigator.clipboard) {
            alert("can't copy, press f12 and copy from console")
            return;
        }
        navigator.clipboard.writeText(text).then(function() {
            alert("copied")
        }, _ => {
            alert("can't copy, press f12 and copy from console")
        });
    }

    return (
        <div className="view result" ref={self}>
            <h2>Yuor job finished!</h2>
            <button className="out" onClick={copyTextToClipboard}>Copy names</button>
        </div>
    )
}

export default Result