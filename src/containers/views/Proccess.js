import React, { useEffect, useRef, useState } from 'react';
import {useSelector, useDispatch} from 'react-redux';

import Dot from "../components/Dot"

const Proccess = () => {

    const self = useRef(null)
    const image = useRef(null)

    const [loaded, setLoaded] = useState(false)
    const [startCount, setStartCount] = useState(0)

    const active = useSelector(state => state.files.active)
    const files = useSelector(state => state.files.files)
    const limit = useSelector(state => state.files.limit)
    const count = useSelector(state => state.files.count)

    const dispatch = useDispatch()

    useEffect(() => {
        if (loaded) return
        setLoaded(true)
        setTimeout(() => {
            self.current.style.opacity = "1"
        }, 10)
        setStartCount(count)
        if (image.current.clientHeight > image.current.clientWidth) {
            image.current.classList.remove("horizontal")
        } else {
            if (!image.current.classList.contains("horizontal")) {
                image.current.classList.add("horizontal")
            }
        }
    }, [loaded, dispatch, startCount, count])

    useEffect(() => {
        if (loaded) return

        const proccessImageChange = changer => {
            image.current.style.opacity = "0"
            setTimeout(() => {
                changer()
                setTimeout(() => {
                    if (image.current.clientHeight > image.current.clientWidth) {
                        image.current.classList.remove("horizontal")
                    } else {
                        if (!image.current.classList.contains("horizontal")) {
                            image.current.classList.add("horizontal")
                        }
                    }
                }, 25)
                setTimeout(() => {
                    image.current.style.opacity = "1"
                }, 50)
            }, 300)
        }

        const keyPress = event => {
            switch (event.keyCode) {
                case 39:
                    //right
                    proccessImageChange(() => {dispatch({type: "NextImage"})})
                    break
                case 37:
                    //left
                    proccessImageChange(() => {dispatch({type: "PrevImage"})})
                    break
                case 32:
                    if (files.length > limit) proccessImageChange(() => {dispatch({type: "RemoveActive"})})
                    //space
                    break
                default:
                    return
            }
        }

        window.onkeyup = keyPress
    }, [loaded, dispatch, files.length, limit])

    useEffect(() => {
        if (files.length === limit) {
            self.current.style.opacity = "0"
            setTimeout(() => {
                dispatch({type: "SetView", value: "result"})
            }, 500)
        }
    }, [files.length, limit, dispatch])

    const dots = () => {
        var d = []
        for (var i = 0; i < limit; i++) {
            d.push((
                <Dot index={i} max={limit}></Dot>
            ))
        }
        return d
    }

    return (
        <div className="view proccess" ref={self}>
            <img className="display" alt="current" src={files[active].img} ref={image}></img>
            {dots().map(v => v)}
            <div className="progress" style={{width: `${(startCount - count)/(startCount - limit)*100}vw`}}></div>
        </div>
    )
}

export default Proccess