import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

const maxLimit = 25

const Settings = () => {

    const self = useRef(null)

    const [loaded, setLoaded] = useState(false)
    const [clickStart, setClickStart] = useState(0)
    const [clicked, setClicked] = useState(false)
    const [startLimit, setStartLimit] = useState(0)

    const limit = useSelector(state => state.files.limit)
    const count = useSelector(state => state.files.count)
    const files = useSelector(state => state.files.files)

    const dispatch = useDispatch()

    useEffect(() => {
        setLoaded(true)
        dispatch({type: "SetCount", value: files.length})
        console.log(files.length)
        setTimeout(() => {
            self.current.style.opacity = "1"
        }, 10)
    }, [loaded, dispatch, files.length])

    const spinnerDown = event => {
        setClickStart(event.pageY)
        setClicked(true)
        setStartLimit(limit)
    }

    const mouseMove = event => {
        if (!clicked) return
        var newLimit = startLimit + Math.round((clickStart - event.pageY) / 10)
        if (newLimit < 1) return
        if (newLimit > count || newLimit > maxLimit) return
        dispatch({type: "SetLimit", value: newLimit})
        if (event.pageY < 10 || event.pageY > window.clientHeigth - 10) setClicked(false)
    }

    const mouseUp = () => {
        setClicked(false)
    }

    const start = () => {
        self.current.style.opacity = "0"
        setTimeout(() => {
            if (window.localStorage.getItem("teached")) {
                dispatch({type: "SetView", value: "proccess"})
            } else {
                dispatch({type: "SetView", value: "teaching"})
            }
        }, 500)
    }

    return (
        <div className="view settings" onMouseMove={mouseMove} onMouseUp={mouseUp} ref={self}>
            <div className="spinner out" onMouseDown={spinnerDown}>
                {limit}
                <svg width="100" height="100" viewBox="0 0 100 100" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path 
                        d="
                            M 50, 50
                            m 0, -40
                            a 40 40 0 0 1 40 40
                            a 40 40 0 0 1 -40 40
                            a 40 40 0 0 1 -40 -40
                            a 40 40 0 0 1 40 -40
                        "
                        fill="none" stroke="#ffaa4f" strokeWidth="5"
                        strokeDasharray={`${40*2*Math.PI * limit / ((count > maxLimit) ? maxLimit : count)}, ${40*2*Math.PI * (1 - limit / ((count > maxLimit) ? maxLimit : count))}`}
                    />
                </svg>
            </div>
            <h2 className="top">I want to choose</h2>
            <h2 className="bottom">photos.</h2>
            <button className="start out" onClick={start}>start</button>
        </div>
    )
}

export default Settings