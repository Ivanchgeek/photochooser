import React, {useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import upload_src from "../../stuff/upload.png"

const Upload = () => {

    const inputFiles = useRef(null)
    const inputButton = useRef(null)
    const instruction = useRef(null)

    const [chosen, setChosen] = useState(false)

    const dispatch = useDispatch()

    const uploadClick = () => {
        if (!chosen) inputFiles.current.click()
    }

    const filesChosen = () => {
        setChosen(true)
        var files = []
        for (var i = 0; i < inputFiles.current.files.length; i++) {
            var f = inputFiles.current.files[i]
            if (!f.type.match('image.*')) {
                alert("all files should be images")
                window.location.reload()
            }
            var reader = new FileReader()
            reader.onload = event => {
                files.push({img: event.target.result, name: event.target.fileName})
                if (files.length === inputFiles.current.files.length) {
                    dispatch({type: "SetFiles", value: files})
                    instruction.current.style.opacity = "0"
                    setTimeout(() => {
                        dispatch({type: "SetView", value: "settings"})
                    }, 1000)
                }
            }
            reader.fileName = f.name
            reader.readAsDataURL(f);
        }
    }

    return (
        <div className="view upload">
            <h2 ref={instruction}>Choose your images</h2>
            <button className={`upload ${chosen ? "" : "out"}`} onClick={uploadClick} ref={inputButton}><img src={upload_src} alt="upload"/></button>
            <input type="file" multiple ref={inputFiles} onChange={filesChosen}></input>
        </div>
    )
}

export default Upload